﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DynamicCountPanels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicCountPanels.Tests
{
    [TestClass()]
    public class CalcTests
    {
        [TestMethod()]
        public void CreateTest1()
        {
            // arrange
            Calc calc = new Calc();
            int N = 1;
            var expected = (1,1);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest2()
        {
            // arrange
            Calc calc = new Calc();
            int N = 2;
            var expected = (1, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest3()
        {
            // arrange
            Calc calc = new Calc();
            int N = 3;
            var expected = (2, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest4()
        {
            // arrange
            Calc calc = new Calc();
            int N = 4;
            var expected = (2, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest5()
        {
            // arrange
            Calc calc = new Calc();
            int N = 5;
            var expected = (2, 3);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest6()
        {
            // arrange
            Calc calc = new Calc();
            int N = 6;
            var expected = (2, 3);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest7()
        {
            // arrange
            Calc calc = new Calc();
            int N = 7;
            var expected = (3, 3);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void CreateTest1V()
        {
            // arrange
            Calc calc = new Calc() { Orientation = Orientation.Vertical };
            int N = 1;
            var expected = (1, 1);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest2V()
        {
            // arrange
            Calc calc = new Calc() { Orientation = Orientation.Vertical };
            int N = 2;
            var expected = (2, 1);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest3V()
        {
            // arrange
            Calc calc = new Calc() { Orientation = Orientation.Vertical };
            int N = 3;
            var expected = (2, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest4V()
        {
            // arrange
            Calc calc = new Calc() { Orientation = Orientation.Vertical };
            int N = 4;
            var expected = (2, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest5V()
        {
            // arrange
            Calc calc = new Calc() { Orientation = Orientation.Vertical };
            int N = 5;
            var expected = (3, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest6V()
        {
            // arrange
            Calc calc = new Calc() { Orientation = Orientation.Vertical };
            int N = 6;
            var expected = (3, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest7V()
        {
            // arrange
            Calc calc = new Calc() { Orientation = Orientation.Vertical };
            int N = 7;
            var expected = (3, 3);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void CreateTest1RM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomRow };
            int N = 1;
            var expected = (1, 1);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest2RM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomRow };
            int N = 2;
            var expected = (2, 1);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest3RM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomRow };
            int N = 3;
            var expected = (2, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest4RM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomRow };
            int N = 4;
            var expected = (2, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest5RM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomRow };
            int N = 5;
            var expected = (2, 3);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest6RM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomRow };
            int N = 6;
            var expected = (2, 3);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest7RM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomRow };
            int N = 7;
            var expected = (2, 4);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }



        [TestMethod()]
        public void CreateTest1CM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomColumn };
            int N = 1;
            var expected = (1, 1);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTestCM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomColumn };
            int N = 2;
            var expected = (1, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest3CM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomColumn };
            int N = 3;
            var expected = (2, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest4CM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomColumn };
            int N = 4;
            var expected = (2, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest5CM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomColumn };
            int N = 5;
            var expected = (3, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest6CM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomColumn };
            int N = 6;
            var expected = (3, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CreateTest7CM2()
        {
            // arrange
            Calc calc = new Calc() { Dimension = Dimension.CustomColumn };
            int N = 7;
            var expected = (4, 2);

            // act
            var actual = calc.CalcDim(N);

            // assert
            Assert.AreEqual(expected, actual);
        }


    }
}