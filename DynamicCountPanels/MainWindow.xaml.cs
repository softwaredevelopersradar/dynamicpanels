﻿using ChartUserControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TestUserControl;

namespace DynamicCountPanels
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Calc calc = new Calc();

        //DynamicPanels<StackPanelControl> dynamicPanels;
        DynamicPanels<ChartControl> dynamicPanels;

        public MainWindow()
        {
            InitializeComponent();
            //dynamicPanels = new DynamicPanels<StackPanelControl>(DynamicGrid);
            dynamicPanels = new DynamicPanels<ChartControl>(DynamicGrid);
        }

        int N = 0;
        Random random = new Random();

        int currentCount = 0;
        (int rowCount, int columnCount) currentDim = (0, 0);

        private void DoIt()
        {
            N++;

            button.Content = N;

            int desiredCount = N;
            var desiredDim = calc.CalcDim(N);

            DynamicGrid.RowDefinitions.Clear();
            DynamicGrid.ColumnDefinitions.Clear();

            DynamicGrid.Children.Clear();

            for (int i = 0; i < desiredDim.rowCount; i++)
            {
                RowDefinition rowDef = new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) };
                DynamicGrid.RowDefinitions.Add(rowDef);
            }

            for (int i = 0; i < desiredDim.columnCount; i++)
            {
                ColumnDefinition colDef = new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) };
                DynamicGrid.ColumnDefinitions.Add(colDef);
            }

            currentCount = N;
            currentDim = desiredDim;

            for (int i = 0; i < desiredDim.rowCount; i++)
                for (int j = 0; j < desiredDim.columnCount; j++)
                {
                    StackPanel stackPanel = new StackPanel()
                    {
                        Background = new SolidColorBrush(Color.FromRgb((byte)random.Next(255), (byte)random.Next(255), (byte)random.Next(255)))
                    };

                    Grid.SetRow(stackPanel, i);
                    Grid.SetColumn(stackPanel, j);

                    DynamicGrid.Children.Add(stackPanel);

                    if (DynamicGrid.Children.Count == N) return;

                }



        }

        private void DoIt3<T>(int n) where T : UserControl, new()
        {
            if (n <= 0) n = 0;

            if (n == 0)
            {
                DynamicGrid.Children.Clear();

                DynamicGrid.RowDefinitions.Clear();
                DynamicGrid.ColumnDefinitions.Clear();

                currentCount = 0;
                currentDim = (0, 0);

                return;
            }

            int desiredCount = n;
            var desiredDim = calc.CalcDim(n);

            if (desiredCount < currentCount)
            {
                int needToDeleteChildrensCount = currentCount - desiredCount;
                if (needToDeleteChildrensCount != 0)
                    DynamicGrid.Children.RemoveRange(DynamicGrid.Children.Count - needToDeleteChildrensCount, needToDeleteChildrensCount);

                currentCount = desiredCount;

                int needToDeleteRowsCount = currentDim.rowCount - desiredDim.rowCount;
                if (needToDeleteRowsCount != 0)
                    DynamicGrid.RowDefinitions.RemoveRange(DynamicGrid.RowDefinitions.Count - needToDeleteRowsCount, needToDeleteRowsCount);

                int needToDeleteColumnsCount = currentDim.columnCount - desiredDim.columnCount;
                if (needToDeleteColumnsCount != 0)
                    DynamicGrid.ColumnDefinitions.RemoveRange(DynamicGrid.ColumnDefinitions.Count - needToDeleteColumnsCount, needToDeleteColumnsCount);

                currentDim = desiredDim;

                int w = 0;
                for (int i = 0; i < desiredDim.rowCount; i++)
                {
                    for (int j = 0; j < desiredDim.columnCount; j++)
                    {
                        int rowIndex = Grid.GetRow(DynamicGrid.Children[w]);
                        int columnIndex = Grid.GetColumn(DynamicGrid.Children[w]);

                        if (rowIndex != i || columnIndex != j)
                        {
                            Grid.SetRow(DynamicGrid.Children[w], i);
                            Grid.SetColumn(DynamicGrid.Children[w], j);
                        }

                        w++;

                        if (w == DynamicGrid.Children.Count) return;
                    }
                }
            }

            if (desiredCount > currentCount)
            {
                int needToAddRowsCount = desiredDim.rowCount - currentDim.rowCount;
                for (int i = 0; i < needToAddRowsCount; i++)
                {
                    RowDefinition rowDef = new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) };
                    DynamicGrid.RowDefinitions.Add(rowDef);
                }

                int needToAddColumnsCount = desiredDim.columnCount - currentDim.columnCount;
                for (int i = 0; i < needToAddColumnsCount; i++)
                {
                    ColumnDefinition colDef = new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) };
                    DynamicGrid.ColumnDefinitions.Add(colDef);
                }

                currentDim = desiredDim;

                int w = 0;
                for (int i = 0; i < desiredDim.rowCount; i++)
                {
                    for (int j = 0; j < desiredDim.columnCount; j++)
                    {
                        if (w < currentCount && currentCount != 0)
                        {
                            int rowIndex = Grid.GetRow(DynamicGrid.Children[w]);
                            int columnIndex = Grid.GetColumn(DynamicGrid.Children[w]);

                            if (rowIndex != i || columnIndex != j)
                            {
                                Grid.SetRow(DynamicGrid.Children[w], i);
                                Grid.SetColumn(DynamicGrid.Children[w], j);
                            }

                            w++;
                        }

                        if (w == currentCount)
                        {
                            w++;
                            if (desiredCount != 1 && currentCount != 0)
                                continue;
                        }

                        if (w > currentCount)
                        {
                            T t = new T();
                            t.MouseDown += StackPanel_MouseDown;

                            Grid.SetRow(t, i);
                            Grid.SetColumn(t, j);

                            DynamicGrid.Children.Add(t);

                            if (DynamicGrid.Children.Count == desiredCount)
                            {
                                currentCount = desiredCount;
                                return;
                            }
                        }
                    }
                }
            }
        }


        private void DoIt2(int n)
        {
            if (n <= 0) n = 0;

            if (n == 0)
            {
                DynamicGrid.Children.Clear();

                DynamicGrid.RowDefinitions.Clear();
                DynamicGrid.ColumnDefinitions.Clear();

                currentCount = 0;
                currentDim = (0, 0);

                return;
            }

            int desiredCount = n;
            var desiredDim = calc.CalcDim(n);

            if (desiredCount < currentCount)
            {
                int needToDeleteChildrensCount = currentCount - desiredCount;
                if (needToDeleteChildrensCount != 0)
                    DynamicGrid.Children.RemoveRange(DynamicGrid.Children.Count - needToDeleteChildrensCount, needToDeleteChildrensCount);

                currentCount = desiredCount;

                int needToDeleteRowsCount = currentDim.rowCount - desiredDim.rowCount;
                if (needToDeleteRowsCount != 0)
                    DynamicGrid.RowDefinitions.RemoveRange(DynamicGrid.RowDefinitions.Count - needToDeleteRowsCount, needToDeleteRowsCount);

                int needToDeleteColumnsCount = currentDim.columnCount - desiredDim.columnCount;
                if (needToDeleteColumnsCount != 0)
                    DynamicGrid.ColumnDefinitions.RemoveRange(DynamicGrid.ColumnDefinitions.Count - needToDeleteColumnsCount, needToDeleteColumnsCount);

                currentDim = desiredDim;

                int w = 0;
                for (int i = 0; i < desiredDim.rowCount; i++)
                {
                    for (int j = 0; j < desiredDim.columnCount; j++)
                    {
                        int rowIndex = Grid.GetRow(DynamicGrid.Children[w]);
                        int columnIndex = Grid.GetColumn(DynamicGrid.Children[w]);

                        if (rowIndex != i || columnIndex != j)
                        {
                            Grid.SetRow(DynamicGrid.Children[w], i);
                            Grid.SetColumn(DynamicGrid.Children[w], j);
                        }

                        w++;

                        if (w == DynamicGrid.Children.Count) return;
                    }
                }
            }

            if (desiredCount > currentCount)
            {
                int needToAddRowsCount = desiredDim.rowCount - currentDim.rowCount;
                for (int i = 0; i < needToAddRowsCount; i++)
                {
                    RowDefinition rowDef = new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) };
                    DynamicGrid.RowDefinitions.Add(rowDef);
                }

                int needToAddColumnsCount = desiredDim.columnCount - currentDim.columnCount;
                for (int i = 0; i < needToAddColumnsCount; i++)
                {
                    ColumnDefinition colDef = new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) };
                    DynamicGrid.ColumnDefinitions.Add(colDef);
                }

                currentDim = desiredDim;

                int w = 0;
                for (int i = 0; i < desiredDim.rowCount; i++)
                {
                    for (int j = 0; j < desiredDim.columnCount; j++)
                    {
                        if (w < currentCount && currentCount != 0 )
                        {
                            int rowIndex = Grid.GetRow(DynamicGrid.Children[w]);
                            int columnIndex = Grid.GetColumn(DynamicGrid.Children[w]);

                            if (rowIndex != i || columnIndex != j)
                            {
                                Grid.SetRow(DynamicGrid.Children[w], i);
                                Grid.SetColumn(DynamicGrid.Children[w], j);
                            }

                            w++;
                        }

                        if (w == currentCount)
                        {
                            w++;
                            if (desiredCount != 1 && currentCount != 0)
                                continue;
                        }

                        if (w > currentCount)
                        {
                            StackPanel stackPanel = new StackPanel()
                            {
                                Background = new SolidColorBrush(Color.FromRgb((byte)random.Next(255), (byte)random.Next(255), (byte)random.Next(255)))
                            };

                            stackPanel.MouseDown += StackPanel_MouseDown;  

                            Grid.SetRow(stackPanel, i);
                            Grid.SetColumn(stackPanel, j);

                            DynamicGrid.Children.Add(stackPanel);

                            if (DynamicGrid.Children.Count == desiredCount)
                            {
                                currentCount = desiredCount;
                                return;
                            }
                        }
                    }
                }
            }
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var index = DynamicGrid.Children.IndexOf(sender as StackPanel);
            Console.WriteLine(index);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DoIt2(Convert.ToInt32(tb.Text));
        }

        private void Button_Click_Swap(object sender, RoutedEventArgs e)
        {

            /*
                int s1 = random.Next(0, DynamicGrid.Children.Count);
                int s2 = random.Next(0, DynamicGrid.Children.Count);

                var r1 = Grid.GetRow(DynamicGrid.Children[s1]);
                var c1 = Grid.GetColumn(DynamicGrid.Children[s1]);

                var r2 = Grid.GetRow(DynamicGrid.Children[s2]);
                var c2 = Grid.GetColumn(DynamicGrid.Children[s2]);

                Grid.SetRow(DynamicGrid.Children[s2], r1);
                Grid.SetColumn(DynamicGrid.Children[s2], c1);

                Grid.SetRow(DynamicGrid.Children[s1], r2);
                Grid.SetColumn(DynamicGrid.Children[s1], c2);
            */
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            //DoIt3<StackPanelControl>(Convert.ToInt32(tb.Text));

            dynamicPanels.DoIt3(Convert.ToInt32(tb.Text));

            //dynamicPanels[random.Next(0, dynamicPanels.Count())].ChangeColor(Color.FromRgb((byte)random.Next(255), (byte)random.Next(255), (byte)random.Next(255)));

            //var indexedChild = DynamicGrid.Children[random.Next(0, DynamicGrid.Children.Count)] as StackPanelControl;
            //indexedChild.ChangeColor(Color.FromRgb((byte)random.Next(255), (byte)random.Next(255), (byte)random.Next(255)));
        }
    }
}
