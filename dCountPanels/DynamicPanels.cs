﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace dCountPanels
{
    public class DynamicPanels<T> where T : UserControl, new()
    {
        private Grid DynamicGrid;

        private int currentCount = 0;
        private (int rowCount, int columnCount) currentDim = (0, 0);

        private Calc _calc;

        public DynamicPanels(Grid EmptyGrid, Calc calc = null)
        {
            this.DynamicGrid = EmptyGrid;
            if (calc == null)
            {
                _calc = new Calc();
            }
            else
            {
                _calc = calc;
            }
        }

        private List<T> Ts = new List<T>();

        private static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        public void DoIt(int n)
        {
            semaphoreSlim.Wait();

            if (n <= 0) n = 0;

            if (n == 0)
            {
                DynamicGrid.Children.Clear();
                Ts.Clear();

                DynamicGrid.RowDefinitions.Clear();
                DynamicGrid.ColumnDefinitions.Clear();

                currentCount = 0;
                currentDim = (0, 0);

                semaphoreSlim.Release();
                return;
            }

            int desiredCount = n;
            var desiredDim = _calc.CalcDim(n);

            if (desiredCount < currentCount)
            {
                int needToDeleteChildrensCount = currentCount - desiredCount;
                if (needToDeleteChildrensCount != 0)
                {
                    int childrenCount = DynamicGrid.Children.Count;
                    DynamicGrid.Children.RemoveRange(childrenCount - needToDeleteChildrensCount, needToDeleteChildrensCount);
                    Ts.RemoveRange(childrenCount - needToDeleteChildrensCount, needToDeleteChildrensCount);
                }

                currentCount = desiredCount;

                int needToDeleteRowsCount = currentDim.rowCount - desiredDim.rowCount;
                if (needToDeleteRowsCount != 0)
                    DynamicGrid.RowDefinitions.RemoveRange(DynamicGrid.RowDefinitions.Count - needToDeleteRowsCount, needToDeleteRowsCount);

                int needToDeleteColumnsCount = currentDim.columnCount - desiredDim.columnCount;
                if (needToDeleteColumnsCount != 0)
                    DynamicGrid.ColumnDefinitions.RemoveRange(DynamicGrid.ColumnDefinitions.Count - needToDeleteColumnsCount, needToDeleteColumnsCount);

                currentDim = desiredDim;

                int w = 0;
                for (int i = 0; i < desiredDim.rowCount; i++)
                {
                    for (int j = 0; j < desiredDim.columnCount; j++)
                    {
                        int rowIndex = Grid.GetRow(DynamicGrid.Children[w]);
                        int columnIndex = Grid.GetColumn(DynamicGrid.Children[w]);

                        if (rowIndex != i || columnIndex != j)
                        {
                            Grid.SetRow(DynamicGrid.Children[w], i);
                            Grid.SetColumn(DynamicGrid.Children[w], j);
                        }

                        w++;

                        if (w == DynamicGrid.Children.Count)
                        {
                            semaphoreSlim.Release();
                            return;
                        }
                    }
                }
            }

            if (desiredCount > currentCount)
            {
                int needToAddRowsCount = desiredDim.rowCount - currentDim.rowCount;
                for (int i = 0; i < needToAddRowsCount; i++)
                {
                    RowDefinition rowDef = new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) };
                    DynamicGrid.RowDefinitions.Add(rowDef);
                }

                int needToAddColumnsCount = desiredDim.columnCount - currentDim.columnCount;
                for (int i = 0; i < needToAddColumnsCount; i++)
                {
                    ColumnDefinition colDef = new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) };
                    DynamicGrid.ColumnDefinitions.Add(colDef);
                }

                currentDim = desiredDim;

                int w = 0;
                for (int i = 0; i < desiredDim.rowCount; i++)
                {
                    for (int j = 0; j < desiredDim.columnCount; j++)
                    {
                        if (w < currentCount && currentCount != 0)
                        {
                            int rowIndex = Grid.GetRow(DynamicGrid.Children[w]);
                            int columnIndex = Grid.GetColumn(DynamicGrid.Children[w]);

                            if (rowIndex != i || columnIndex != j)
                            {
                                Grid.SetRow(DynamicGrid.Children[w], i);
                                Grid.SetColumn(DynamicGrid.Children[w], j);
                            }

                            w++;
                        }

                        if (w == currentCount)
                        {
                            w++;
                            if (desiredCount != 1 && currentCount != 0)
                                continue;
                        }

                        if (w > currentCount)
                        {
                            T t = new T();
                            //t.MouseDown += StackPanel_MouseDown;

                            Grid.SetRow(t, i);
                            Grid.SetColumn(t, j);

                            DynamicGrid.Children.Add(t);
                            Ts.Add(t);

                            if (DynamicGrid.Children.Count == desiredCount)
                            {
                                currentCount = desiredCount;
                                semaphoreSlim.Release();
                                return;
                            }
                        }
                    }
                }
            }

            semaphoreSlim.Release();
        }


        // Define the indexer to allow client code to use [] notation.
        public T this[int i]
        {
            get { return Ts[i]; }
            private set
            {
                DynamicGrid.Children[i] = value;
                Ts[i] = value;
            }
        }

        public int Count()
        {
            return Ts.Count;
        }

    }
}
