﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dCountPanels
{
    public class Calc
    {
        public Orientation Orientation { get; set; } = Orientation.Horizontal;

        public Dimension Dimension { get; set; } = Dimension.Default;

        public int RowMaxCount { get; set; } = 2;
        public int ColumnMaxCount { get; set; } = 2;


        public (int rowCount, int columnCount) CalcDim(int CountPanels)
        {
            int rowCount = 1;
            int columnCount = 1;

            double sqrt = Math.Sqrt(CountPanels);

            int maxRowCount = (int)Math.Ceiling(sqrt);
            int maxColumnCount = (int)Math.Ceiling(sqrt);

            if (Dimension == Dimension.CustomRow)
            {
                maxRowCount = RowMaxCount;
                maxColumnCount = (int)Math.Ceiling((double)CountPanels / (double)maxRowCount);
            }

            if (Dimension == Dimension.CustomColumn)
            {
                maxColumnCount = ColumnMaxCount;
                maxRowCount = (int)Math.Ceiling((double)CountPanels / (double)maxColumnCount);
            }

            while (isEnough(CountPanels, rowCount, columnCount) == false)
            {
                if (Orientation == Orientation.Horizontal)
                {
                    if (columnCount < maxColumnCount)
                    {
                        columnCount++;
                        continue;
                    }
                    if (rowCount < maxRowCount)
                    {
                        rowCount++;
                    }
                }
                if (Orientation == Orientation.Vertical)
                {
                    if (rowCount < maxRowCount)
                    {
                        rowCount++;
                        continue;
                    }
                    if (columnCount < maxColumnCount)
                    {
                        columnCount++;
                    }
                }
            }
            return (rowCount, columnCount);
        }

        private bool isEnough(int totalNumber, int dim1, int dim2)
        {
            if (totalNumber <= dim1 * dim2)
                return true;
            else return false;
        }
    }

    public enum Orientation
    {
        Horizontal,
        Vertical
    }

    public enum Dimension
    {
        Default,
        CustomRow,
        CustomColumn
    }
}
