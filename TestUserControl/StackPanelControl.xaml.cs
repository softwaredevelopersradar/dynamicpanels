﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestUserControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class StackPanelControl : UserControl
    {

        public StackPanelControl()
        {
            InitializeComponent();
            sp.Background = new SolidColorBrush(Colors.Gray);
        }

        public void ChangeColor(Color color)
        {
            sp.Background = new SolidColorBrush(color);
        }

        public StackPanelControl(Color color)
        {
            InitializeComponent();
            sp.Background = new SolidColorBrush(color);
        }
    }
}
